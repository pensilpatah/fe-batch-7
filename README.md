# fe-batch-7

<h1>FE Basic 1</h1>

- Our Silabus
    - Front End Silabus Bacth 
    ````
    https://docs.google.com/document/d/1S2Znol5qJPjUbRkK3WT6sSBnD-GjqmlLH8dMtRZJ9hQ/edit
    ````
- First Read about this book
    - Eloquent Javascript
    ````
    http://eloquentjavascript.net/
    ````
- What is Front End?
    ````
    https://medium.com/@makersinstitute/front-end-back-end-full-stack-apa-artinya-36e0f25e8142
    ````
- Tech in Front End?
    - jQuery
    ````
     https://en.wikipedia.org/wiki/JQuery
    ````
    - AngularJS
    ````
    https://en.wikipedia.org/wiki/AngularJS
    ````
    - ReactJS
    ````
    https://en.wikipedia.org/wiki/React_(JavaScript_library)
    ````
    - VueJS
    ````
    https://en.wikipedia.org/wiki/Vue.js
    ````
- What must learn in Front End?
    - Programming Language
        - HTML
            - Struktur
        - CSS
            - Visual
        - JavaScript
            - Fungsional
            - Animasi 
    - Data Communication
        - ajax
        ````
        https://en.wikipedia.org/wiki/Ajax_(programming)
        ````
- Tools
    - Browser
        - Google Chrome
        ````
        https://www.google.com/chrome/
        ````
    - IDE
        - Visual Studio Code
        ````
        https://code.visualstudio.com/download
        ````
    - Fancy Tools
        - Wakatime
        ````
        https://wakatime.com
        ````
- Javascript
    - Version
        ````
        https://www.w3schools.com/js/js_versions.asp
        ````
    - Installation
        - Node.js
        ````
        https://nodejs.org/en/download/
        ````
    - Playground
        ````
        https://jsbin.com/
        ````
- Basic Code
    - Data Type
        - Dynamic Data Type
            - Undefined
            - Null
            - Empty
            - Number
            - Special Number (float)
            - String
            - Booleans
            - Objects
                - Object
                - Array
            - typeof operator
        ````
        https://www.w3schools.com/js/js_datatypes.asp
        ````
    - Arithmetic
        - Addition (+)
        - Substraction (-)
        - Multiplication (*)
        - Division (/)
        - Remainder (%)
        - Increment (++)
        - Decrement (—)
- Bonus : Git
    - Buat Akun Gitlab
    - Share Akun dengan Saya (@ariefdfaltah)
    - Clone Repository : https://gitlab.com/ariefdfaltah/FE-BASIC-1.git
    - Buat Branch dengan nama kalian dan pindah ke Branch Tersebut
    - Isi Data berikut :
        - Nama :
        - Asal :
        - Username Gitlab :
    - Push Branch tersebut ke Remote Repo
    
- Bonus : Video
    - CSS : https://www.youtube.com/watch?v=yfoY53QXEnI

- Latihan : Data Type, Looping, Conditional
    - Case : 
    ````
    //PR
    //var dataPR = [1,2,"mangga",7,4,2,4,5,"tata",82,4,1]
    //coba tentukan element array tersebut bilangan genap, bilangan ganjil atau string.

    length = untuk menghitung jumlah dar index array

    =============================
    Materi, 5 Maret 2018
    =============================
    var a = 1; //data type "number"
    var b = "dua"; //data type "string"

    // console.log(a)
    // console.log(b)

    var aa = undefined; //data type "undefined"
    var bb = null; //data type "null"
    var cc = ""; //data type "empty"

    // console.log(aa == bb);
    // console.log(bb == cc);
    // console.log(aa == cc);

    var special = 123e5;
    // console.log(special);

    var bArr = [ 1, "adi", 3, 4, "tata", 7];

    // console.log(bArr[4]);

    var cObj = { 
      'tata': 'javascript', 
     'aji': 'python' 
    };

    // console.log(cObj['tata']);

    // console.log(1 + 1);
    // console.log(1 - 1);
    // console.log(1 + "a");
    // console.log(10 * 10);
    // console.log(90 / 10);
    // console.log(3 % 2);
    // console.log(1++); 
    // console.log(1--);

    // console.log(11 % 8);

    var dataArr = [1,2,3,5,6,23]

    for(var i=0; i < dataArr.length; i++) {
    //   console.log("Ini Data ke " + i + " Dengan Nilai " + dataArr[i]);
    }

    var angka = 5;

    if(angka % 2 == 0) {
      console.log("Angka Merupakan Bilangan Genap");
    } else {
     console.log("Angka Merupakan Bilangan Ganjil");
    }

    =============================
    End of Materi
    =============================
````
#AUTHOR : TATA 